﻿using System.IO;

namespace StreamComparer.Utils
{
	public class ChannelConfig
	{
		public string Name { get; set; }
		public int DriveSize { get; set; }
		public string RootPath { get; set; }
		public string AdsDumpPath { get; set; }
		public string AdsStorePath { get; set; }
		public string TranscriptPath { get; set; }
		public string[] AdsPaths { get; set; }

		public string RootStreamPath { get; set; }
		public string AudioStreamPath { get; set; }
		public string VideoStreamPath { get; set; }

	}
}
