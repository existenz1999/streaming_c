﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using StreamComparer.Core;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StreamComparer.Utils
{
   public class EmguReader
	{

      public static EmguGrayVideo ReadWholeVideo(string path) 
      {
         var videoName = Path.GetFileNameWithoutExtension(path);

         var videoCapture = new VideoCapture(path);
         int frameCount = (int)Math.Floor(videoCapture.GetCaptureProperty(CapProp.FrameCount));
         var grayVideo = new EmguGrayVideo(videoName, frameCount);

         if (videoCapture != null)
         {
            int frameCounter = 0;
            while (true)
            {
               var currentFrame = videoCapture.QueryFrame();
               if (currentFrame != null)
               {
                  grayVideo.ChainFrame(new EmguGrayImage(currentFrame, $"{videoName}_seg_{++frameCounter}")
                     .Downsample(ProcessConfigurator.DownscaleRatio)
                     .Clusterize(ProcessConfigurator.KMeansClustersCount));
               }
               else break;
            }
         }

         return grayVideo;
      }

      public static EmguGrayVideo ReadVideoKeyFrames(string path) 
      {
         var videoName = Path.GetFileNameWithoutExtension(path);
         var keyFramesPaths = Directory.GetFiles(path);

         var grayVideo = new EmguGrayVideo(videoName, keyFramesPaths.Length);

         Parallel.ForEach(keyFramesPaths, (framePath) => 
         {
            int frameIdx = int.Parse(Regex.Match(Path.GetFileNameWithoutExtension(framePath), @"\d+").Value) - 1;
            grayVideo.ChainFrame(ReadImage(framePath), frameIdx);
         });

         return grayVideo;
      }

      public static EmguGrayImage ReadImage(string path) => 
         new EmguGrayImage(CvInvoke.Imread(path), Path.GetFileNameWithoutExtension(path))
         .Downsample(ProcessConfigurator.DownscaleRatio)
         .Clusterize(ProcessConfigurator.KMeansClustersCount);

      public static void SaveImage(EmguGrayImage image, string path) => image.Mat.Save(path);
   }
}
