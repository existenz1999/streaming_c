﻿namespace StreamComparer.Utils
{
	public class ProcessConfigurator
	{
		public static string ChannelsDirName { get; } = "channels";
		public static string AdsDirName { get; } = "ads";
		public static string AdsDumpDirName { get; } = "ads_dump";
		public static string AdsStoreDirName { get; } = "ads_store";
		public static string StreamDirName { get; } = "stream";
		public static string AudioDirName { get; } = "audio";
		public static string VideoDirName { get; } = "video";
		public static string TranscriptFileName { get; } = "transcript.txt";
		public static char RAMDiskDrive { get; } = 'R';
		public static int AdAquisitionWindowInterval { get; } = 60;
		public static int RAMDiskSize { get; } = 512;
		public static int DownscaleRatio { get; } = 5;
		public static int KMeansClustersCount { get; } = 4;
		public static double HighCorrelationThreshold { get; } = 0.75;
		public static double LowCorrelationThreshold { get; } = 0.35;
		public static double AudioAccuracyThreshold { get; } = 0.8;
		public static int KConsecutiveFramesTolerance { get; } = 1; //DO NOT CHANGE THIS UNTIL TESTS ARE COMPLETE
		public static string BackendDomain { get; set; } = "https://localhost:44382/";
		public static string UpdateChannelRoute { get; set; } = "api/v1/channel";
		public static string RootDir { get; set; }
	}
}
