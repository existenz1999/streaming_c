﻿using System;

namespace StreamComparer.Utils
{
	public class ChannelRequestDto
	{
		public ChannelRequestDto() { }

		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsAd { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}
