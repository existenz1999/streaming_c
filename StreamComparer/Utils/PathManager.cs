﻿using StreamComparer.Processing;
using System;
using System.IO;

namespace StreamComparer.Utils
{
	public enum Channel
	{
		DIGI_24,
		HBO
	}
	public class PathManager
	{
		private static readonly Lazy<PathManager> manager = new Lazy<PathManager>(() => new PathManager());
		public static PathManager Instance { get { return manager.Value; } }
		
		private string channelsDirPath;
		private PathManager()
		{
			this.channelsDirPath = Path.Combine(ProcessConfigurator.RootDir, ProcessConfigurator.ChannelsDirName);
			if (!Directory.Exists(channelsDirPath)) Directory.CreateDirectory(channelsDirPath);
		}


		public ChannelConfig CreateChannelConfig(Channel c) 
		{
			var channelName = Enum.GetName(typeof(Channel), c).ToLower();
			var channelDirPath = Path.Combine(channelsDirPath, channelName);
			var ramDiskDrive = ProcessConfigurator.RAMDiskDrive;

			var ramDiskPath = Path.Combine($"{char.ToString(ramDiskDrive)}:", ProcessConfigurator.StreamDirName);
			var audioPath = Path.Combine(ramDiskPath, Path.Combine(channelName, ProcessConfigurator.AudioDirName));
			var videoPath = Path.Combine(ramDiskPath, Path.Combine(channelName, ProcessConfigurator.VideoDirName));

			var transcriptPath = Path.Combine(channelDirPath, ProcessConfigurator.TranscriptFileName);
			var adsDir = Path.Combine(channelDirPath, ProcessConfigurator.AdsDirName);
			var adsDumpPath = Path.Combine(ramDiskPath, ProcessConfigurator.AdsDumpDirName);
			var adsStorePath = Path.Combine(channelDirPath, ProcessConfigurator.AdsStoreDirName);
			if (!Directory.Exists(adsDir)) Directory.CreateDirectory(adsDir);
			var adsPaths = Directory.GetDirectories(adsDir);

			return new ChannelConfig 
			{
				Name = channelName,
				DriveSize = ProcessConfigurator.RAMDiskSize,
				RootPath = channelDirPath,
				TranscriptPath = transcriptPath,
				RootStreamPath = ramDiskPath,
				VideoStreamPath = videoPath,
				AudioStreamPath = audioPath,
				AdsPaths = adsPaths,
				AdsDumpPath = adsDumpPath,
				AdsStorePath = adsStorePath
			};
		}
		
	}
}
