﻿using StreamComparer.Core;
using StreamComparer.Processing.Exceptions;
using System;
using System.IO;
using System.Threading.Tasks;

namespace StreamComparer.Processing
{
	public class AdManager : ProcessManager
	{
		private string[] adsPaths;
		private string storePath;
		private string dumpPath;
		private EmguGrayChannel channel;
		private EmguAd[] ads;

		public AdManager(string[] adsPaths, string storePath, string dumpPath) : base(dumpPath)
		{
			this.adsPaths = adsPaths;
			this.storePath = storePath;
			this.dumpPath = dumpPath;
		}

		
		public EmguAd[] LoadAds(EmguGrayChannel channel) 
		{
			this.ads = new EmguAd[adsPaths.Length];
			this.channel = channel;
			Parallel.ForEach(adsPaths, (path) => 
			{
				var idx = Array.IndexOf(adsPaths, path);
				ads[idx] = new EmguAd(adsPaths[idx]);
			});
			return ads;
		}

		public override void _StartProcessing(int seconds)
		{
			if (channel != null) 
			{
				commandExecutor.ClearWorkingDir();
				var statusCode = commandExecutor.WriteVideoChunks(channel.StreamUrl, seconds, justKeyFrames: true);
				if (statusCode != 0) throw new ProcessFaultedException();
				else 
				{
					var newFramesFilePaths = Directory.GetFiles(dumpPath);
					var guid = Guid.NewGuid().ToString();
					var newStoreDirPath = Path.Combine(storePath, guid);
					Directory.CreateDirectory(newStoreDirPath);
					foreach (var filePath in newFramesFilePaths) 
					{
						var fileName = Path.GetFileName(filePath);
						var newPath = Path.Combine(newStoreDirPath, fileName);
						File.Copy(filePath, newPath);
					}
					OnProcessStateChanged(ProcessState.FINISHED);
				}
			}
			
		}

		protected override void ProcessChunks(object sender, FileSystemEventArgs e)
		{
		}
	}
}
