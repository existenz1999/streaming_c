﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace StreamComparer.Processing
{
	public class CommandExecutor : IDisposable
	{
		private string workingDir;
		private FileSystemWatcher fileWatcher;


		public CommandExecutor(string workingDir, FileSystemEventHandler onChanged = null)
		{
			this.workingDir = workingDir;
			if (!Directory.Exists(workingDir)) Directory.CreateDirectory(workingDir);
			this.fileWatcher = new FileSystemWatcher(workingDir);
			fileWatcher.EnableRaisingEvents = true;
			fileWatcher.Changed += onChanged;
		}

		public int WriteVideoChunks(string streamUrl, int time = 0, bool justKeyFrames = false) 
		{
			var process = new Process();
			var fileWildCard = "frame%03d.jpg";
			//USE -vf select=\"eq(pict_type\\, PICT_TYPE_I)\" FOR SELECTING ONLY KEYFRAMES
			var getChunksCommand = $"/C ffmpeg -i \"{streamUrl}\" {(time == 0 ? string.Empty : "-t " + time)} {(justKeyFrames ? "-vf select=\"eq(pict_type\\, PICT_TYPE_I)\"" : string.Empty)} -an -q:v 2 -vsync 0 {Path.Combine(workingDir, fileWildCard)}";
			process.StartInfo = BuildProcessInfo(getChunksCommand);
			process.StartInfo.WorkingDirectory = workingDir;
			process.Start();
			process.WaitForExit();
			return process.ExitCode;
		}

		public int WriteAudioChunks(string streamUrl, int time = 0)
		{
			var process = new Process();
			var fileWildCard = "audio%03d.flac";
			var getChunksCommand = $"/C ffmpeg -i \"{streamUrl}\" {(time == 0 ? string.Empty : "-t " + time)} -vn -q:a 0 -acodec flac -f segment -segment_time 5 {Path.Combine(workingDir, fileWildCard)}";
			process.StartInfo = BuildProcessInfo(getChunksCommand);
			process.StartInfo.WorkingDirectory = workingDir;
			process.Start();
			process.WaitForExit();
			return process.ExitCode;
		}

		public static void CreateRAMDisk(char driveLetter, int mbSize) 
		{	
			var drives = DriveInfo.GetDrives().ToList();
			if (!drives.Any(d => d.Name.ToLower().Contains(char.ToString(driveLetter).ToLower()))) 
			{
				var process = new Process();
				var ramDiskCommand = $"/C ramdiskc /add:{char.ToString(driveLetter)} /size:{mbSize}m /fs:fat32";
				process.StartInfo = BuildProcessInfo(ramDiskCommand);
				process.Start();
				process.WaitForExit();
			}
		}

		public static void DeleteRAMDisk(char driveLetter)
		{
			var drives = DriveInfo.GetDrives().ToList();
			if (drives.Any(d => d.Name.ToLower().Contains(char.ToString(driveLetter).ToLower()))) 
			{
				var process = new Process();
				var ramDiskCommand = $"/C ramdiskc /del:{char.ToString(driveLetter)} /force";
				process.StartInfo = BuildProcessInfo(ramDiskCommand);
				process.Start();
				process.WaitForExit();
			}
		}

		public static int StopAll() 
		{
			var process = new Process();
			var killFfmpegCommand = "/C Taskkill /F /IM ffmpeg.exe";
			process.StartInfo = BuildProcessInfo(killFfmpegCommand);
			process.Start();
			process.WaitForExit();
			return process.ExitCode;
		}

		private static ProcessStartInfo BuildProcessInfo(string command)
		{
			var psi = new ProcessStartInfo()
			{
				FileName = "cmd.exe",
				UseShellExecute = false,
				RedirectStandardOutput = true,
				WindowStyle = ProcessWindowStyle.Hidden,
				CreateNoWindow = true,
				Arguments = command
			};
			return psi;
		}

		public void ClearWorkingDir()
		{
			var dirInfo = new DirectoryInfo(this.workingDir);
			foreach (var file in dirInfo.EnumerateFiles())
			{
				file.Delete();
			}
			foreach (var dir in dirInfo.EnumerateDirectories())
			{
				dir.Delete(true);
			}
		}

		public void Dispose() 
		{
			fileWatcher.Dispose();
		}
	}
}
