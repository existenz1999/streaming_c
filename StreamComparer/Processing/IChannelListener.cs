﻿using System;
using System.Threading.Tasks;

namespace StreamComparer.Processing
{
	public interface IChannelListener
	{
		Task ChannelUpdated(string name, string content, bool isAd, DateTime updateTime);
	}
}
