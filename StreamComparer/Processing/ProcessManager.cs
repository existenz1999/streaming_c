﻿using StreamComparer.Processing.Exceptions;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Schedulers;

namespace StreamComparer.Processing
{
	public enum ProcessState { INITIALIZED = 1, STARTED = 2, FINISHED = 3, PAUSED = 4, ERRORED = 5 }
	public abstract class ProcessManager : IDisposable
	{
		private static QueuedTaskScheduler queueScheduler = new QueuedTaskScheduler(targetScheduler: TaskScheduler.Default);
		public static TaskScheduler highPriorityTs = queueScheduler.ActivateNewQueue(1);
		public static TaskScheduler middlePriority = queueScheduler.ActivateNewQueue(2);
		public static TaskScheduler lowPriorityTs = queueScheduler.ActivateNewQueue(3);

		private Thread processThread;
		protected string outputDir;
		protected CommandExecutor commandExecutor;
		protected CancellationTokenSource cts;
		protected volatile int handlersNum = 0;
		private volatile static bool signaledToStop = false;

		public delegate void ProcessUpdate(string content, bool isAd, DateTime updateTime);
		public delegate void ProcessStateChange(ProcessState state);
		public event ProcessUpdate ProcessUpdated;
		public event ProcessStateChange ProcessStateChanged;
		protected void OnProcessUpdated(string content, bool isAd) => ProcessUpdated?.Invoke(content, isAd, DateTime.Now);
		protected void OnProcessStateChanged(ProcessState state) 
		{
			this.State = state;
			ProcessStateChanged?.Invoke(state);
		}


		public ProcessState State { get; private set; }

		public ProcessManager(string outputDir) 
		{
			this.commandExecutor = new CommandExecutor(outputDir, ProcessChunks);
			this.outputDir = outputDir;
			this.State = ProcessState.INITIALIZED;
		}

		protected abstract void ProcessChunks(object sender, FileSystemEventArgs e);

		public void StartProcessing(int seconds) 
		{
			if (State != ProcessState.STARTED)
			{
				OnProcessStateChanged(ProcessState.STARTED);
				cts = new CancellationTokenSource();
				processThread = new Thread(() =>
				{
					try
					{
						_StartProcessing(seconds);
					}
					catch (Exception ex)
					{
						if (ex is ProcessFaultedException || ex is ThreadInterruptedException) 
						{
							if (signaledToStop) 
							{
								OnProcessStateChanged(ProcessState.PAUSED);
								return;
							}
						}
						OnProcessStateChanged(ProcessState.ERRORED);
					}
				});
				processThread.Start();
			}
		}

		public abstract void _StartProcessing(int seconds);

		public virtual void StopProcessing() 
		{
			try
			{
				signaledToStop = true;
				if (State == ProcessState.STARTED)
				{
					CommandExecutor.StopAll();
					cts.Cancel();
					processThread.Interrupt();
					commandExecutor.ClearWorkingDir();
					OnProcessStateChanged(ProcessState.PAUSED);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				signaledToStop = false;
			}
		}
		
		public virtual void Dispose()
		{
			signaledToStop = true;
			processThread.Interrupt();
			commandExecutor.Dispose();
		}
	}
}
