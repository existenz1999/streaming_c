﻿using Google.Api.Gax.Grpc;
using Google.Cloud.Speech.V1;
using StreamComparer.Utils;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace StreamComparer.Processing
{
	internal class GoogleAudioClassificator
	{
		private SpeechClient speechRequester;
		private RecognitionConfig audioConfig;
		private string transcriptPath;
		public GoogleAudioClassificator(string transcriptPath)
		{
			this.speechRequester = SpeechClient.Create();
			this.audioConfig = new RecognitionConfig
			{
				Encoding = RecognitionConfig.Types.AudioEncoding.Flac,
				SampleRateHertz = 48000,
				LanguageCode = LanguageCodes.Romanian.Romania,
				AudioChannelCount = 2
			};
			this.transcriptPath = transcriptPath;
		}

		public void SaveTranscript(string filePath, DateTime transcriptTime, CancellationToken token = default) 
		{
			try
			{
				token.ThrowIfCancellationRequested();
				var audio = RecognitionAudio.FromFile(filePath);
				var callOptions = CallSettings.FromCancellationToken(token);
				var response = speechRequester.Recognize(audioConfig, audio, callOptions);
				foreach (var result in response.Results)
				{
					foreach (var alternative in result.Alternatives)
					{
						if (alternative.Confidence >= ProcessConfigurator.AudioAccuracyThreshold)
						{
							var transcript = alternative.Transcript;
							Console.WriteLine("Transcript is: " + transcript);
							var text = $"{transcriptTime.ToString("[yyyy, MMMM, dddd - HH:mm:ss.ff]")} {transcript} \n";
							var textBytes = new UTF8Encoding(true).GetBytes(text);
							using (var transcriptStream = File.Open(transcriptPath, FileMode.Append, FileAccess.Write, FileShare.Read))
							{
								transcriptStream.Write(textBytes, 0, textBytes.Length);
								break;
							}
						}
					}
				}
			}
			catch (Exception ex) 
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
