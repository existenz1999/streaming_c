﻿using StreamComparer.Processing.Exceptions;
using StreamComparer.Utils;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StreamComparer.Processing
{
	internal class AudioProcessManager : ProcessManager
	{
		private GoogleAudioClassificator classificator;
		private string streamUrl;
		public AudioProcessManager(string streamUrl, string outputDir, GoogleAudioClassificator classificator) : base(outputDir)
		{
			this.streamUrl = streamUrl;
			this.classificator = classificator;
		}

		public override void _StartProcessing(int seconds) 
		{
			commandExecutor.ClearWorkingDir();
			var statusCode = commandExecutor.WriteAudioChunks(streamUrl, seconds);
			if (statusCode != 0) throw new ProcessFaultedException();
		}


		protected override void ProcessChunks(object sender, FileSystemEventArgs e)
		{
			if (State == ProcessState.STARTED) 
			{
				Task.Factory.StartNew(() =>
				{
					try
					{
						Interlocked.Increment(ref handlersNum);
						var audioChunkPath = e.FullPath;
						var fileInfo = new FileInfo(audioChunkPath);
						if (fileInfo.Length > 0)
						{
							cts.Token.ThrowIfCancellationRequested();
							classificator.SaveTranscript(audioChunkPath, fileInfo.LastWriteTimeUtc, cts.Token);
							File.Delete(audioChunkPath);
						}
					}
					catch (Exception ex)
					{
						if (ex is OperationCanceledException) { }
						else
							Console.WriteLine(ex.Message);
					}
					finally 
					{
						if (Interlocked.Decrement(ref handlersNum) == 0)
						{
							OnProcessStateChanged(ProcessState.FINISHED);
						}
					}
				}, cts.Token, TaskCreationOptions.DenyChildAttach, middlePriority);
			}
		}
	}
}
