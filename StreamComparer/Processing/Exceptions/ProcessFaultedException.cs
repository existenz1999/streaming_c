﻿using System;

namespace StreamComparer.Processing.Exceptions
{
   public class ProcessFaultedException : Exception
   {
      public ProcessFaultedException()
      {
      }

      public ProcessFaultedException(string message) : base(message)
      {
      }

      public ProcessFaultedException(string message, Exception inner) : base(message, inner)
      {
      }
   }
}
