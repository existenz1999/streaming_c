﻿using Newtonsoft.Json;
using StreamComparer.Utils;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StreamComparer.Processing
{
	public class BackendRequester : IChannelListener
	{
		private string backendDomain;
		private readonly HttpClient client = new HttpClient();

		public BackendRequester(string backendDomain)
		{
			this.backendDomain = backendDomain;
		}

		private async Task UpdateChannel(string uri, ChannelRequestDto dto) 
		{
			string channelJson = JsonConvert.SerializeObject(dto);
			var content = new StringContent(channelJson, Encoding.UTF8, "application/json");
			var response = await client.PostAsync(backendDomain + uri, content);
		}

		public async Task ChannelUpdated(string channelName, string content, bool isAd, DateTime updateTime)
		{
			var channelRequestDto = new ChannelRequestDto
			{
				Name = channelName,
				Description = content,
				IsAd = isAd,
				UpdatedAt = updateTime
			};
			await UpdateChannel(ProcessConfigurator.UpdateChannelRoute, channelRequestDto);
		}
	}
}
