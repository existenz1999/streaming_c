﻿using StreamComparer.Core;
using StreamComparer.Processing.Exceptions;
using StreamComparer.Utils;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StreamComparer.Processing
{
	internal class VideoProcessManager : ProcessManager
	{
		private EmguAd[] ads;
		private string streamUrl;

		public VideoProcessManager(string streamUrl, string outputDir, EmguAd[] ads) : base(outputDir)
		{
			this.streamUrl = streamUrl;
			this.ads = ads;
		}

		public override void _StartProcessing(int seconds)
		{
			commandExecutor.ClearWorkingDir();
			var statusCode = commandExecutor.WriteVideoChunks(streamUrl, seconds);
			if (statusCode != 0) throw new ProcessFaultedException();
		}


		protected override void ProcessChunks(object sender, FileSystemEventArgs e) 
		{
			if (State == ProcessState.STARTED) 
			{
				Task.Factory.StartNew(() =>
				{
					try
					{
						Interlocked.Increment(ref handlersNum);
						var framePath = e.FullPath;
						var fileInfo = new FileInfo(framePath);
						if (fileInfo.Length > 0)
						{
							var frame = EmguReader.ReadImage(framePath);
							var parallelOptions = new ParallelOptions
							{
								CancellationToken = cts.Token
							};
							cts.Token.ThrowIfCancellationRequested();
							Parallel.ForEach(ads, parallelOptions, (EmguAd adC, ParallelLoopState state) =>
							{
								if (cts.Token.IsCancellationRequested)
									state.Break();
								var isFrameInAd = adC.IsFrameInAdSequence(frame, cts.Token);
								if (isFrameInAd)
								{
									OnProcessUpdated(adC.ToString(), true);
									Console.WriteLine($"Found {frame.Name} in {adC.Video.Name}");
								}
							});
							File.Delete(framePath);
						}
					}
					catch (Exception ex)
					{
						if (ex is OperationCanceledException) { }
						else
							Console.WriteLine(ex.Message);
					}
					finally 
					{
						if (Interlocked.Decrement(ref handlersNum) == 0) 
						{
							OnProcessStateChanged(ProcessState.FINISHED);
						}
					}
				}, cts.Token, TaskCreationOptions.DenyChildAttach, lowPriorityTs);
			}	
		}
	}
}
