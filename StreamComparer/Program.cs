﻿using StreamComparer.Core;
using StreamComparer.Processing;
using StreamComparer.Utils;
using System;
using System.Threading.Tasks;

namespace StreamComparer
{
   class Program
	{
      static EmguGrayChannel digiChannel;
      static async Task Main(string[] args)
      {
         await Task.Delay(2000);
         var streamUrl = "https://edge211.rcs-rds.ro/V4-5826136-ac78d48266ae988b58ed61d1cfb189b2-3585153083-0-1608542055-4-digi24/digi24/digi24-abr.m3u8?is=4&ns=digi24&p=Browser&s=site&sn=digionline.ro&t=00000000000000000000000000000000&t4=V4-5826136-ac78d48266ae988b58ed61d1cfb189b2-3585153083-0-1608542055-4-digi24";
         var rootDir = @"D:\07_DevProjects\Streams\hls";
         ProcessConfigurator.RootDir = rootDir;

         var backendRequester = new BackendRequester(ProcessConfigurator.BackendDomain);
         var channelConfig = PathManager.Instance.CreateChannelConfig(Channel.DIGI_24);
         digiChannel = new EmguGrayChannel(streamUrl, channelConfig);
         digiChannel.AddListener(backendRequester);

         digiChannel.ChannelStateChanged += ChannelStateChanged;
         await digiChannel.StartAdAquisition(1);

         Console.Read();
      }

      private static void ChannelStateChanged(ChannelState state)
      {
         Console.WriteLine(state);

      }
   }
}
