﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using StreamComparer.Utils;
using System;
using System.Drawing;

namespace StreamComparer.Core
{
	public class EmguGrayImage
	{
		private Image<Gray, float> _image;
		public Mat Mat { get; private set; }
		public Image<Gray, float> Image 
		{ 
			get 
			{
				if (_image == null)
					_image = Mat.ToImage<Gray, float>();
				return _image;
			}
		}
		public int Rows { get => Image.Rows; }
		public int Cols { get => Image.Cols; }
		public Size Size { get => Image.Size; }
		public string Name { get; private set; }

		public EmguGrayImage(Mat mat, string name)
		{
			var gray = new Mat();
			if (mat.Dims == 3) CvInvoke.CvtColor(mat, gray, ColorConversion.Rgb2Gray);
			else gray = mat;

			this.Mat = gray;
			this.Name = name; 
		}


		public EmguGrayImage Downsample(int scale)
		{
			CvInvoke.Resize(this.Mat, this.Mat, Size.Empty, (1.0 / scale), (1.0 / scale), Inter.Nearest);
			this._image = this.Mat.ToImage<Gray, float>();
			return this;
		}

		public EmguGrayImage Clusterize(int kClusterCount)
		{
			var flattened = new Matrix<float>(Rows * Cols, 1, 1);
			for (int y = 0; y < Rows; y++)
			{
				for (int x = 0; x < Cols; x++)
				{
					flattened.Data[y + x * Rows, 0] = (float)_image[y, x].Intensity;
				}
			}

			var term = new MCvTermCriteria(10, 1.0);
			term.Type = TermCritType.Eps & TermCritType.Iter;

			var centers = new Matrix<float>(kClusterCount, flattened.Cols, 1);
			var clusters = new Matrix<int>(Rows * Cols, 1);
			CvInvoke.Kmeans(flattened, kClusterCount, clusters, term, 3, KMeansInitType.RandomCenters, centers);

			for (int y = 0; y < Rows; y++)
			{
				for (int x = 0; x < Cols; x++)
				{
					int clusterIdx = clusters[y + x * Rows, 0];
					MCvScalar intensity = CvInvoke.cvGet2D(centers, clusterIdx, 0);
					_image[y, x] = new Gray((float)intensity.V0);
				}
			}

			this.Mat = _image.Mat;

			return this;
		}

		public double SimilarityWith(EmguGrayImage otherImage)
		{
			var mean = new MCvScalar();
			var otherMean = new MCvScalar();
			var std = new MCvScalar();
			var otherStd = new MCvScalar();

			CvInvoke.MeanStdDev(this.Mat, ref mean, ref std);
			CvInvoke.MeanStdDev(otherImage.Mat, ref otherMean, ref otherStd);

			var stdAB = std.V0 * otherStd.V0;
			if (stdAB == 0) return 0;

			var meanDiff = this.Mat - mean.V0;
			var otherMeanDiff = otherImage.Mat - otherMean.V0;
			var pixelCount = Rows * Cols;
			var diffDiffIm = (meanDiff - otherMeanDiff).ToImage<Gray, float>();
			var greaterThanKSum = 0.0;

			for (int y = 0; y < Rows; y++)
			{
				for (int x = 0; x < Cols; x++)
				{
					var intensity = diffDiffIm[y, x].Intensity;
					if (intensity > 5) greaterThanKSum += intensity;
				}
			}
			var differenceRatio = 1 - (greaterThanKSum / pixelCount);

			var diffMult = new Mat();
			CvInvoke.Multiply(meanDiff, otherMeanDiff, diffMult, dtype: DepthType.Cv32F);
			var covariance = CvInvoke.Mean(diffMult);
			var correlationidx = covariance.V0 / stdAB;

			if (correlationidx < ProcessConfigurator.LowCorrelationThreshold) return correlationidx;
			else return Math.Max(correlationidx, differenceRatio);
		}

	}
}
