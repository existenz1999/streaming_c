﻿using StreamComparer.Utils;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StreamComparer.Core
{
   public class EmguAd
	{
      public EmguGrayVideo Video { get; private set; }

      public EmguAd(string adPath)
      {
         Video = EmguReader.ReadVideoKeyFrames(adPath);
      }

      public bool IsFrameInAdSequence(EmguGrayImage frame, CancellationToken token = default) 
      {
         var simillarities = new double[Video.FrameCount];
         var parallelOptions = new ParallelOptions 
         {
            CancellationToken = token
         };
         try 
         {
            token.ThrowIfCancellationRequested();
            Parallel.ForEach(Video.Frames, parallelOptions, (EmguGrayImage seqFrame, ParallelLoopState loopState) =>
            {
               if (token.IsCancellationRequested)
                  loopState.Break();
               var simillarity = frame.SimilarityWith(seqFrame);
               simillarities[Video.GetFrameIndex(seqFrame)] = simillarity;
            });
         } catch (Exception ex) 
         {
            Console.WriteLine(ex.Message);
         }
         //Console.WriteLine($"Highest Correlation for frame {frame.Name} was: {simillarities.Max()}");
         var consecutiveCorrelation = LongestRepeatingChain(simillarities, ProcessConfigurator.HighCorrelationThreshold);
         return consecutiveCorrelation >= ProcessConfigurator.KConsecutiveFramesTolerance;
      }

      private static int LongestRepeatingChain(double[] arr, double k)
      {
         var length = 0;
         var currentLength = 0;

         for (int i = 0; i < arr.Length; i++) 
         {
            if (arr[i] >= k) currentLength++;
            else currentLength = 0;
            length = Math.Max(currentLength, length);
         }

         return length;
      }

   }
}
