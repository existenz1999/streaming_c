﻿using System;

namespace StreamComparer.Core
{
	public class EmguGrayVideo
	{
		private int orderedFramesCount = 0;
		public string Name { get; private set; }
		public EmguGrayImage[] Frames { get; private set; }

		public int FrameCount { get; private set; }

		public EmguGrayVideo(string name, int frameCount)
		{
			this.Name = name;
			this.FrameCount = frameCount;
			this.Frames = new EmguGrayImage[frameCount];
		}

		public void ChainFrame(EmguGrayImage frame, int index) => Frames[index] = frame;

		public void ChainFrame(EmguGrayImage frame) => Frames[orderedFramesCount++] = frame;

		public int GetFrameIndex(EmguGrayImage frame) => Array.IndexOf(Frames, frame);
	}
}
