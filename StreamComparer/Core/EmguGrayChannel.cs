﻿using StreamComparer.Processing;
using StreamComparer.Utils;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StreamComparer.Core
{
	public enum ChannelState { INITIALIZED = 1, DOWNSTREAMING = 2, FINISHED = 3, AUDIO_FAULTED = 4, VIDEO_FAULTED = 5, ERRORED = 6, PAUSED = 7}
	public class EmguGrayChannel : IDisposable
	{
		private VideoProcessManager videoManager;
		private AudioProcessManager audioManager;
		private List<IChannelListener> channelListeners;
		private AdManager adManager;
		private CancellationTokenSource adAquisitionCts;

		public ChannelState State { get; private set; }
		public string Name { get; private set; }
		public string StreamUrl { get; private set; }

		public delegate void ChannelStateChange(ChannelState state);
		public event ChannelStateChange ChannelStateChanged;

		public EmguGrayChannel(string streamUrl, ChannelConfig channelConfig)
		{
			this.channelListeners = new List<IChannelListener>();
			this.StreamUrl = streamUrl;

			var ramDiskDrive = ProcessConfigurator.RAMDiskDrive;
			CommandExecutor.CreateRAMDisk(ramDiskDrive, channelConfig.DriveSize);

			this.adManager = new AdManager(channelConfig.AdsPaths, channelConfig.AdsStorePath, channelConfig.AdsDumpPath);
			var ads = adManager.LoadAds(this);

			var audioClassificator = new GoogleAudioClassificator(channelConfig.TranscriptPath);
			this.videoManager = new VideoProcessManager(streamUrl, channelConfig.VideoStreamPath, ads);
			this.audioManager = new AudioProcessManager(streamUrl, channelConfig.AudioStreamPath, audioClassificator);
			this.videoManager.ProcessUpdated += ChannelUpdatedEvent;
			this.videoManager.ProcessStateChanged += ProcessStateChanged;
			this.audioManager.ProcessStateChanged += ProcessStateChanged;

			this.Name = channelConfig.Name;
			this.State = ChannelState.INITIALIZED;
		}

		public async Task StartAdAquisition(int everyNMinutes)
		{
			this.adAquisitionCts = new CancellationTokenSource();
			while (!adAquisitionCts.Token.IsCancellationRequested)
			{
				if (!adAquisitionCts.Token.IsCancellationRequested)
					adManager.StartProcessing(ProcessConfigurator.AdAquisitionWindowInterval);
				await Task.Delay(new TimeSpan(0, everyNMinutes, 0), adAquisitionCts.Token);
			}
		}

		public void StopAdAquisition() 
		{
			adAquisitionCts.Cancel();
			adManager.StopProcessing();
		}

		private void ProcessStateChanged(ProcessState state)
		{
			var videoProcessorState = videoManager.State;
			var audioProcessorState = audioManager.State;
			ChannelState newState = 0;
			if (videoProcessorState == ProcessState.ERRORED || audioProcessorState == ProcessState.ERRORED)
			{
				if (videoProcessorState == ProcessState.ERRORED && audioProcessorState == ProcessState.ERRORED) newState = ChannelState.ERRORED;
				else if (videoProcessorState == ProcessState.ERRORED) newState = ChannelState.VIDEO_FAULTED;
				else if (audioProcessorState == ProcessState.ERRORED) newState = ChannelState.AUDIO_FAULTED;
			}
			else if (videoProcessorState == ProcessState.STARTED && audioProcessorState == ProcessState.STARTED) newState = ChannelState.DOWNSTREAMING;
			else if (videoProcessorState == ProcessState.PAUSED && audioProcessorState == ProcessState.PAUSED) newState = ChannelState.PAUSED;
			else if (videoProcessorState == ProcessState.FINISHED && audioProcessorState == ProcessState.FINISHED) newState = ChannelState.FINISHED;
			if (State != newState && newState != 0) 
			{
				State = newState;
				ChannelStateChanged?.Invoke(State);
			}
		}

		private void ChannelUpdatedEvent(string content, bool isAd, DateTime updateTime)
		{
			foreach (var listener in channelListeners)
			{
				Task.Factory.StartNew(() => listener.ChannelUpdated(Name, content, isAd, updateTime), default, TaskCreationOptions.DenyChildAttach, ProcessManager.highPriorityTs);
			}
		}


		public void AddListener(IChannelListener listener) => channelListeners.Add(listener);

		public void DownStream(int seconds = 0) 
		{
			videoManager.StartProcessing(seconds);
			audioManager.StartProcessing(seconds);
		}

		public void PauseStream() 
		{
			videoManager.StopProcessing();
			audioManager.StopProcessing();
		}

		public void Dispose()
		{
			videoManager.Dispose();
			audioManager.Dispose();
			var ramDiskDrive = ProcessConfigurator.RAMDiskDrive;
			CommandExecutor.DeleteRAMDisk(ramDiskDrive);
		}
	}
}
