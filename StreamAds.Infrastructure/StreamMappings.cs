﻿using AutoMapper;
using StreamAds.Infrastructure.Domain;
using StreamAds.Infrastructure.Dto;

namespace StreamAds.Infrastructure
{
	public class StreamMappings : Profile
	{
		public StreamMappings()
		{
			CreateMap<Channel, ChannelDto>();
		}
	}
}
