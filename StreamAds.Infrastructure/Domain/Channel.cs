﻿using StreamAds.Infrastructure.RepositoryTools;
using System;

namespace StreamAds.Infrastructure.Domain
{
	public class Channel : Entity<long>
	{
		public Channel() { }

		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsAd { get; set; }
		public DateTime? UpdatedAt { get; set; }
	}
}
