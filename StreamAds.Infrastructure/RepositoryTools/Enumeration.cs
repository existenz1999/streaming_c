﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace StreamAds.Infrastructure.RepositoryTools
{
	public abstract class Enumeration : Entity<int>
	{
		private static readonly ConcurrentDictionary<Type, IEnumerable<Enumeration>> CachedEnums = new ConcurrentDictionary<Type, IEnumerable<Enumeration>>();

		protected Enumeration(int id, string name)
		{
			this.Id = id;
			this.Name = name;
		}

		public string Name { get; private set; }

		private static IEnumerable<T> GetAllImpl<T>(Type type) where T : Enumeration
		{
			var fields = Assembly
				 .GetAssembly(type)
				 .GetTypes()
				 .Where(t => t.IsSubclassOf(type) || t == type)
				 .SelectMany(t => GetPropertiesAndFields(t, BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly));

			return fields.Select(f =>
			{
				var value = (T)GetValue(f, null);
				value.Name = f.Name;
				return value;
			});
		}

		private static object GetValue(MemberInfo memberInfo, object obj)
		{
			if (memberInfo is PropertyInfo asProp) return asProp.GetValue(obj);

			if (memberInfo is FieldInfo asField) return asField.GetValue(obj);

			if (memberInfo is MethodInfo asMethod) return asMethod.Invoke(obj, new object[0]);

			throw new Exception("Member not supported");
		}

		private static IEnumerable<MemberInfo> GetPropertiesAndFields(Type type, BindingFlags flags) =>
			 type.GetProperties(flags).Cast<MemberInfo>().Concat(type.GetFields(flags));

		public static IEnumerable<T> GetAll<T>() where T : Enumeration
		{
			return CachedEnums.GetOrAdd(typeof(T), enumType => GetAllImpl<T>(enumType)).Cast<T>();
		}

		public static T FromId<T>(int id) where T : Enumeration
		{
			var matchingItem = GetAll<T>().FirstOrDefault(item => item.Id == id);
			return matchingItem;
		}

		public override bool Equals(object obj)
		{
			var otherValue = obj as Enumeration;

			if (otherValue == null)
				return false;

			var typeMatches = this.GetType().Equals(obj.GetType());
			var valueMatches = this.Id.Equals(otherValue.Id);

			return typeMatches && valueMatches;
		}

		public override int GetHashCode() => this.Id.GetHashCode();

		public int CompareTo(object other) => this.Id.CompareTo(((Enumeration)other).Id);

		public override string ToString() => this.Name;
	}
}
