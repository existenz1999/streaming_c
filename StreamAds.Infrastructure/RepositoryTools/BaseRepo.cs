﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StreamAds.Infrastructure.RepositoryTools
{
	public abstract class BaseRepo<TRepo> : DbContext where TRepo : DbContext
	{
		public BaseRepo(DbContextOptions<TRepo> contextOptions) : base(contextOptions) { }

		private IDbContextTransaction currentTransaction;
		public virtual async Task<TEntity> GetByIdAsync<TEntity, TKey>(TKey entityId)
			where TEntity : BaseEntity
			where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
		{
			return await FindAsync<TEntity>(entityId);
		}

		public virtual TEntity AddEntity<TEntity>(TEntity entity) where TEntity : BaseEntity => Add(entity).Entity;

		public virtual void AddRangeEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity => AddRange(entities);

		public virtual TEntity UpdateEntity<TEntity>(TEntity entity) where TEntity : BaseEntity => Update(entity).Entity;

		public virtual TEntity DeleteEntity<TEntity>(TEntity entity) where TEntity : BaseEntity => Remove(entity).Entity;
		public virtual void DeleteRangeEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity => RemoveRange(entities);

		public virtual async Task Save() => await SaveChangesAsync();

		public virtual async Task<IEnumerable<TResult>> GetAllAsync<TEntity, TResult>(
			Expression<Func<TEntity, TResult>> resultsSelector,
			Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
			Expression<Func<TEntity, bool>> filterBy = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			int? pageNumber = default,
			int? pageSize = default,
			bool disableTracking = true,
			bool distinct = false) where TEntity : BaseEntity => await GetAll(resultsSelector, includeChildren, filterBy, orderBy, pageNumber, pageSize, disableTracking, distinct).ToListAsync();


		public virtual async Task<TResult> GetFirstOrDefaultAsync<TEntity, TResult>(
			Expression<Func<TEntity, TResult>> resultsSelector = null,
			Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
			Expression<Func<TEntity, bool>> filterBy = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			bool disableTracking = true) where TEntity : BaseEntity => await GetAll(resultsSelector, includeChildren, filterBy, orderBy, disableTracking: disableTracking).FirstOrDefaultAsync();

		public virtual async Task<TResult> GetSingleOrDefaultAsync<TEntity, TResult>(
			Expression<Func<TEntity, TResult>> resultsSelector = null,
			Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
			Expression<Func<TEntity, bool>> filterBy = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			bool disableTracking = true) where TEntity : BaseEntity => await GetAll(resultsSelector, includeChildren, filterBy, orderBy, disableTracking: disableTracking).SingleOrDefaultAsync();

		public IQueryable<TResult> GetAll<TEntity, TResult>(
			Expression<Func<TEntity, TResult>> resultsSelector,
			Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
			Expression<Func<TEntity, bool>> filterBy = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			int? pageNumber = default,
			int? pageSize = default,
			bool disableTracking = true,
			bool distinct = false) where TEntity : BaseEntity
		{
			var query = Set<TEntity>() as IQueryable<TEntity>;
			if (disableTracking)
			{
				query = query.AsNoTracking();
			}

			return AdvancedSearchForGivenCollection(query, resultsSelector, includeChildren, filterBy, orderBy, pageNumber, pageSize, distinct);
		}

		public async Task BeginTransactionAsync()
		{
			currentTransaction = currentTransaction ?? await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);
		}

		public async Task CommitTransactionAsync()
		{
			try
			{
				await SaveChangesAsync();
				currentTransaction.Commit();
			}
			catch (Exception ex)
			{
				RollbackTransaction();
				throw ex;
			}
			finally
			{
				if (currentTransaction != null)
				{
					currentTransaction?.Dispose();
					currentTransaction = null;
				}
			}
		}

		private void RollbackTransaction()
		{
			try
			{
				currentTransaction?.Rollback();
			}
			finally
			{
				if (currentTransaction != null)
				{
					currentTransaction?.Dispose();
					currentTransaction = null;
				}
			}
		}

		private static IQueryable<TResult> AdvancedSearchForGivenCollection<TEntity, TResult>(
			IQueryable<TEntity> sourceCollection,
			Expression<Func<TEntity, TResult>> resultsSelector,
			Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = default,
			Expression<Func<TEntity, bool>> filterBy = default,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = default,
			int? pageNumber = default,
			int? pageSize = default,
			bool distinct = false)
		{
			var query = sourceCollection;

			if (includeChildren != null)
			{
				query = includeChildren(query);
			}

			if (filterBy != null)
			{
				query = query.Where(filterBy);
			}

			if (orderBy != null)
			{
				query = orderBy(query);
			}

			if (pageNumber.HasValue && pageSize.HasValue)
			{
				query = query.Skip(pageNumber.Value).Take(pageSize.Value);
			}

			if (distinct)
			{
				query = query.Distinct();
			}

			return query.Select(resultsSelector);
		}
	}
}
