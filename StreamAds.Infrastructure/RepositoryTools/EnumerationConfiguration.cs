﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.Linq;

namespace StreamAds.Infrastructure.RepositoryTools
{
	public abstract class EnumerationConfiguration<T> : IEntityTypeConfiguration<T> where T : Enumeration
	{
		public virtual void Configure(EntityTypeBuilder<T> builder)
		{
			builder.ToTable(typeof(T).Name);

			builder.HasKey(col => col.Id);
			builder.Property(col => col.Id).HasDefaultValue(1).ValueGeneratedNever().IsRequired();
			builder.Property(col => col.Name).HasMaxLength(256).IsRequired();

			builder.HasData(SeedData(builder));
		}

		protected virtual IEnumerable<T> SeedData(EntityTypeBuilder<T> builder)
		{
			return Enumeration.GetAll<T>().ToArray();
		}
	}
}
