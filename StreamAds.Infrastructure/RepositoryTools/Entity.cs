﻿using System;

namespace StreamAds.Infrastructure.RepositoryTools
{
	public abstract class Entity<Key> : BaseEntity where Key : IComparable, IComparable<Key>, IEquatable<Key>
	{
		public virtual Key Id { get; set; }
	}
}
