﻿namespace StreamAds.Infrastructure.Dto
{
	public class ChannelDto
	{
		public ChannelDto() { }

		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsAd { get; set; }
	}
}
