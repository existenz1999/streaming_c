﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StreamAds.Infrastructure
{
	public static class DbKeys
	{
		public static string StreamAdDbConnectionKey { get; } = "Stream";
	}
}
