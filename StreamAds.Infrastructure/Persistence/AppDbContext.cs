﻿using Microsoft.EntityFrameworkCore;
using StreamAds.Infrastructure.Persistence.Configurations;
using StreamAds.Infrastructure.RepositoryTools;

namespace StreamAds.Infrastructure.Persistence
{
	public class AppDbContext : BaseRepo<AppDbContext>
	{
		public AppDbContext(DbContextOptions<AppDbContext> contextOptions) : base(contextOptions)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new ChannelConfig());
			base.OnModelCreating(modelBuilder);
		}
	}
}
