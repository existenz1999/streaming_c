﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StreamAds.Infrastructure.Domain;

namespace StreamAds.Infrastructure.Persistence.Configurations
{
	public class ChannelConfig : IEntityTypeConfiguration<Channel>
	{
		public void Configure(EntityTypeBuilder<Channel> builder)
		{
			builder.HasKey(col => col.Id);
			builder.Property(col => col.Id).IsRequired();
			builder.Property(col => col.Name).IsRequired();
			builder.Property(col => col.Description).IsRequired();
			builder.Property(col => col.IsAd).IsRequired();
			builder.Property(col => col.UpdatedAt).IsRequired(false);
		}
	}
}
