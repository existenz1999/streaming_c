﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using StreamAds.API.Commands.CreateChannel;
using StreamAds.API.Commands.UpdateChannel;
using StreamAds.API.ControllerTools;
using StreamAds.API.Queries;
using StreamAds.Infrastructure.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StreamAds.API.Controllers
{
	[ApiController]
	[Route("api/v1/[controller]")]
	public class ChannelController : BaseApiController
	{

		private ChannelQuery channelQuery;
		private IMapper mapper;
		public ChannelController(IMediator mediator, IMapper mapper, ChannelQuery channelQuery) : base(mediator)
		{
			this.mapper = mapper;
			this.channelQuery = channelQuery;
		}

		[HttpGet("version")]
		public async Task<IActionResult> Version()
		{
			return await Task.Run(() =>
			{
				return Ok(new
				{
					ApiName = "Channels",
					ApiVersion = "1.0.0",
					ServiceName = "ChannelService",
					ServiceVersion = "1.0.0"
				});
			});
		}

		[HttpGet]
		public async Task<IActionResult> GetAllChannels()
		{
			var channels = await channelQuery.GetAll();
			var channelDtos = mapper.Map<IEnumerable<ChannelDto>>(channels);
			return Ok(channelDtos);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateChannel([FromBody] UpdateChannel command)
		{
			return Ok(await HandleAsync(command));
		}

		[HttpPost]
		public async Task<IActionResult> CreateChannel([FromBody] CreateChannel command)
		{
			return Ok(await HandleAsync(command));
		}

	}
}
