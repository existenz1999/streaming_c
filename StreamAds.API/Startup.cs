using System;
using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StreamAds.API.Queries;
using StreamAds.Infrastructure;
using StreamAds.Infrastructure.Persistence;

namespace StreamAds.API
{
	public class Startup
	{
		private const string originKey = "CORSALLOW";
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy(originKey,
				builder =>
				{
					builder.AllowAnyOrigin();
				});
			});
			services.AddControllers();
			Assembly infrastructureAssembly = Assembly.GetExecutingAssembly();
			foreach (Type type in infrastructureAssembly.GetTypes())
			{
				if (type.Name.Contains("Handler"))
				{
					services.AddMediatR(type);
				}
			}
			var mappingConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new StreamMappings());
			});

			IMapper mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);
			services.AddDbContext<AppDbContext>(option =>
				{
					var connString = Configuration.GetConnectionString(DbKeys.StreamAdDbConnectionKey);
					option.UseSqlServer(connectionString: connString);
				}
			);
			services.AddScoped<ChannelQuery, ChannelQuery>();
		}


		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseCors(originKey);

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
