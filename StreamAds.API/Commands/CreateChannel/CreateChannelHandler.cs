﻿using StreamAds.API.ControllerTools;
using StreamAds.Infrastructure.Domain;
using StreamAds.Infrastructure.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace StreamAds.API.Commands.CreateChannel
{
	public class CreateChannelHandler : LongContextHandler<CreateChannel>
	{
		public CreateChannelHandler(AppDbContext context) : base(context)
		{

		}


		public override async Task<long> Handle(CreateChannel request, CancellationToken cancellationToken)
		{
			var newChannel = new Channel
			{
				Name = request.Name,
				Description = request.Description,
				IsAd = false,
			};
			context.AddEntity(newChannel);
			await context.Save();
			return newChannel.Id;
		}
	}
}
