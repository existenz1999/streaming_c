﻿using StreamAds.API.ControllerTools;
using System;

namespace StreamAds.API.Commands.CreateChannel
{
	public class CreateChannel : ILongCommand
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsAd { get; set; }
		public DateTime? UpdatedAt { get; set; }
	}
}
