﻿using StreamAds.API.ControllerTools;
using System;

namespace StreamAds.API.Commands.UpdateChannel
{
	public class UpdateChannel : LongUpdateCommand
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsAd { get; set; }
		public DateTime? UpdatedAt { get; set; }
	}
}
