﻿using StreamAds.API.ControllerTools;
using StreamAds.Infrastructure.Domain;
using StreamAds.Infrastructure.Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StreamAds.API.Commands.UpdateChannel
{
	public class UpdateChannelHandler : ContextHandler<UpdateChannel>
	{
		public UpdateChannelHandler(AppDbContext context) : base(context) { }

		protected override async Task Handle(UpdateChannel request, CancellationToken cancellationToken)
		{
			var channel = await context.GetByIdAsync<Channel, long>(request.Id);
			if (channel != null)
			{
				channel.Name = request.Name;
				channel.Description = request.Description;
				channel.IsAd = request.IsAd;
				channel.UpdatedAt = DateTime.Now;
				context.UpdateEntity(channel);
				await context.SaveChangesAsync();
			}
			else
			{
				throw new Exception($"Channel with {request.Id} does not exist");
			}
		}
	}
}
