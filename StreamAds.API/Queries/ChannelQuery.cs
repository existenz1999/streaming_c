﻿using StreamAds.Infrastructure.Domain;
using StreamAds.Infrastructure.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StreamAds.API.Queries
{
	public class ChannelQuery
	{
		protected AppDbContext dbContext;
		public ChannelQuery(AppDbContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public virtual async Task<IEnumerable<Channel>> GetAll()
		{
			var entities = await dbContext.GetAllAsync<Channel, Channel>(
				resultsSelector: q => q
			);
			return entities;
		}

	}
}
