﻿using MediatR;

namespace StreamAds.API.ControllerTools
{
	public interface ILongCommandHandler<TCommandType> : IRequestHandler<TCommandType, long>
		where TCommandType : ILongCommand
	{
	}
}
