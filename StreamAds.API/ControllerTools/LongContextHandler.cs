﻿using StreamAds.Infrastructure.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace StreamAds.API.ControllerTools
{
	public abstract class LongContextHandler<TCommand> : ILongCommandHandler<TCommand> where TCommand : ILongCommand
	{
		protected AppDbContext context;

		public LongContextHandler(AppDbContext context)
		{
			this.context = context;
		}

		public abstract Task<long> Handle(TCommand request, CancellationToken cancellationToken);
	}
}
