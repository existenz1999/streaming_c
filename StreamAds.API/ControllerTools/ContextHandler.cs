﻿using MediatR;
using StreamAds.Infrastructure.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace StreamAds.API.ControllerTools
{
	public abstract class ContextHandler<TContext> : AsyncRequestHandler<TContext> where TContext : IRequest
	{
		protected AppDbContext context;

		public ContextHandler(AppDbContext context)
		{
			this.context = context;
		}

		protected abstract override Task Handle(TContext request, CancellationToken cancellationToken);
	}
}
