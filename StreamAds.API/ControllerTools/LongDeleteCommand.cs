﻿using MediatR;

namespace StreamAds.API.ControllerTools
{
	public abstract class LongDeleteCommand : IRequest
	{
		public long Id { get; set; }
	}
}
