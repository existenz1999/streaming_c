﻿using MediatR;

namespace StreamAds.API.ControllerTools
{
	public abstract class LongUpdateCommand : IRequest
	{
		public long Id { get; set; }
	}
}
