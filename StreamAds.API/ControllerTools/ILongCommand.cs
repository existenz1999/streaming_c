﻿using MediatR;

namespace StreamAds.API.ControllerTools
{
	public interface ILongCommand : IRequest<long> { }
}
