﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace StreamAds.API.ControllerTools
{
	public abstract class BaseApiController : ControllerBase
	{
		private readonly IMediator mediator;
		protected BaseApiController(IMediator mediator)
		{
			this.mediator = mediator;
		}

		protected async Task<TResponse> HandleAsync<TResponse>(IRequest<TResponse> request)
		{
			return await mediator.Send(request);
		}
	}
}
